package br.com.savealife.service;

import java.security.MessageDigest;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.savealife.daos.AtendimentoDao;
import br.com.savealife.daos.EnderecoDao;
import br.com.savealife.daos.InstituicaoDao;
import br.com.savealife.daos.TelefoneDao;
import br.com.savealife.enums.Status;
import br.com.savealife.models.Credencial;
import br.com.savealife.models.Instituicao;

@Service
public class InstituicaoService {
	
	@Autowired
	private InstituicaoDao instituicaoDao;
	
	@Autowired
	private AtendimentoDao atendimentoDao;
	
	@Autowired
	private EnderecoDao enderecoDao;
	
	@Autowired
	private TelefoneDao telefoneDao;
	
	@Transactional
	public void save(Instituicao instituicao) {
		if(instituicao.getEndereco().getId() == null){
			enderecoDao.save(instituicao.getEndereco());
		}
		if(instituicao.getAtendimento().getId() == null){
			atendimentoDao.save(instituicao.getAtendimento());
		}
		if(instituicao.getTelefone().getId() == null){
			telefoneDao.save(instituicao.getTelefone() );
		}
		instituicao.setStatus(Status.S);
		instituicaoDao.save(instituicao);
	}
	
	@Transactional
	public void update(Instituicao instituicao) {
		if(instituicao.getEndereco().getId() != null){
			enderecoDao.update(instituicao.getEndereco());
		}
		if(instituicao.getAtendimento().getId() != null){
			atendimentoDao.update(instituicao.getAtendimento());
		}
		if(instituicao.getTelefone() == null){
			telefoneDao.update(instituicao.getTelefone() );
		}
		instituicao.setStatus(Status.S);
		instituicaoDao.update(instituicao);
	}
	
	public String senhaCriptrografada(String senha){
		MessageDigest algorithm;
		try {
			algorithm = MessageDigest.getInstance("MD5");
			byte messageDigest[] = algorithm.digest("senha".getBytes("UTF-8"));
			StringBuilder hexString = new StringBuilder();
			for (byte b : messageDigest) {
			  hexString.append(String.format("%02X", 0xFF & b));
			}
			return hexString.toString();
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public List<Instituicao> all(){
		return instituicaoDao.all();
	}
	
	public List<Instituicao> listInstituicaoPorCodigo(Long codigo){
		return instituicaoDao.listInstituicaoPorCodigo(codigo);
	}

	public Credencial findCredencais(String email, String senha) {
		return instituicaoDao.findCredencais(email, senha);
	}

	public Instituicao find(Long id) {
		return instituicaoDao.find(id);
	}
}
