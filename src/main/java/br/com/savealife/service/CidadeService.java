package br.com.savealife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.savealife.daos.CidadeDao;
import br.com.savealife.models.Cidade;

@Service
public class CidadeService {
	
	@Autowired
	private CidadeDao cidadeDao;
	
	public List<Cidade> listCidadesPorEstado(Long idEstado){
		return cidadeDao.listCidadesPorEstado(idEstado);
	}

}
