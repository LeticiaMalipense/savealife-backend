package br.com.savealife.service;

import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.savealife.daos.NoticiaDao;
import br.com.savealife.models.Noticia;

@Service
public class NoticiaService {
	@Autowired
	private NoticiaDao noticiaDao;

	public List<Noticia> all(Long id){
      return noticiaDao.all(id);
    }
	
	@Transactional
	public void save(Noticia noticia){
		noticia.setData(new Date());
		noticiaDao.save(noticia);
	}

	@Transactional
	public void delete(Noticia noticia) {
		noticiaDao.delete(noticia);
	}

	@Transactional
	public void update(Noticia noticia) {
		noticiaDao.update(noticia);
	}
	
	public Noticia find(Long id) {
		return noticiaDao.find(id);
	}
}
