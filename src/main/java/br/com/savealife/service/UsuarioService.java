package br.com.savealife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.savealife.daos.UsuarioDao;
import br.com.savealife.models.Usuario;

@Service
public class UsuarioService {
	
	@Autowired
	private UsuarioDao usuarioDao;

	public List<Usuario> all(Usuario usuario) {
		 return usuarioDao.all(usuario);
	}
	public List<Usuario> all() {
		 return usuarioDao.all();
	}
}
