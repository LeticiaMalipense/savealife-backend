package br.com.savealife.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.savealife.daos.EstadoDao;
import br.com.savealife.models.Estado;

@Service
public class EstadoService {
	
	@Autowired
	private EstadoDao estadoDao;
	
	public List<Estado> all(){
		return estadoDao.all();
	}

}
