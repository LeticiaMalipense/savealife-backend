package br.com.savealife.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.savealife.daos.ParceiroDao;
import br.com.savealife.daos.TelefoneDao;
import br.com.savealife.models.Parceiro;

@Service
public class ParceiroService {
	
	@Autowired
	private ParceiroDao parceiroDao;
	
	@Autowired
	private TelefoneDao telefoneDao;

	public List<Parceiro> all(Long id){
      return parceiroDao.all(id);
    }
	
	@Transactional
	public void save(Parceiro parceiro){
		if(parceiro.getTelefone().getId() == null){
			telefoneDao.save(parceiro.getTelefone());
		}
		parceiroDao.save(parceiro);
	}

	@Transactional
	public void delete(Parceiro parceiro) {
		parceiroDao.delete(parceiro);
	}

	@Transactional
	public void update(Parceiro parceiro) {
		parceiroDao.update(parceiro);
	}
	
	public Parceiro find(Long id) {
		return parceiroDao.find(id);
	}

	public List<Parceiro> find(Parceiro parceiro) {
		return parceiroDao.find(parceiro);
	}

}
