package br.com.savealife.enums;

public enum Status {
	
	S("Ativo"), N("Inativo");

	private String descricao;
	
	Status(String descricao) {
		this.descricao = descricao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

}
