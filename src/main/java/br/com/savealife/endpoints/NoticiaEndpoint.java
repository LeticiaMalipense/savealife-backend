package br.com.savealife.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.savealife.models.Noticia;
import br.com.savealife.service.NoticiaService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/noticia")
public class NoticiaEndpoint {

	@Autowired
	private NoticiaService noticiaService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value="{id}")
	public List<Noticia> get(@PathVariable("id") Long id) {
		return noticiaService.all(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void post(@RequestBody Noticia noticia) {
		if(noticia.getCod() == null){
			noticiaService.save(noticia);
		}else{
			noticiaService.update(noticia);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/remove/{id}")
	public void delete(@PathVariable("id") Long id) {
		Noticia noticia = noticiaService.find(id);
		noticiaService.delete(noticia);
	}
}
