package br.com.savealife.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.savealife.models.Cidade;
import br.com.savealife.service.CidadeService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/cidade")
public class CidadeEndpoint {
	
	@Autowired
	private CidadeService cidadeService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{id}")
	public List<Cidade> get(@PathVariable("id") Long idEstado) {
		return cidadeService.listCidadesPorEstado(idEstado);
	}

}
