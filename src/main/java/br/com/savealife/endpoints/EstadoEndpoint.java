package br.com.savealife.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.savealife.models.Estado;
import br.com.savealife.service.EstadoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/estado")
public class EstadoEndpoint {
	
	@Autowired
	private EstadoService estadoService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<Estado> get() {
		return estadoService.all();
	}

}
