package br.com.savealife.endpoints;

import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.savealife.models.Credencial;
import br.com.savealife.service.InstituicaoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/login")
public class LoginEndpoint {
	@Autowired
	private InstituicaoService instituicaoService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value = "/{token}")
	public Credencial get(@PathVariable("token") String token) {
		byte[] decoded = Base64.getDecoder().decode(token);
		String[] login = new String(decoded, StandardCharsets.UTF_8).split("/");
		String senha = instituicaoService.senhaCriptrografada(login[1]);
		return instituicaoService.findCredencais(login[0], senha);
	}
	

}
