package br.com.savealife.endpoints;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.savealife.models.Parceiro;
import br.com.savealife.service.ParceiroService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/parceiro")
public class ParceiroEndpoints {
	
	@Autowired
	private ParceiroService parceiroService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value="{id}")
	public List<Parceiro> get(@PathVariable("id") Long id) {
		return parceiroService.all(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, value = "/filter")
	public List<Parceiro> postFilter(@RequestBody Parceiro parceiro) {
		return parceiroService.find(parceiro);
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void post(@RequestBody Parceiro parceiro) {
		if(parceiro.getId() == null){
			parceiroService.save(parceiro);
		}else{
			parceiroService.update(parceiro);
		}
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/remove/{id}")
	public void delete(@PathVariable("id") Long id) {
		Parceiro parceiro = parceiroService.find(id);
		parceiroService.delete(parceiro);
	}

}
