package br.com.savealife.endpoints;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.com.savealife.models.Instituicao;
import br.com.savealife.service.InstituicaoService;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/instituicao")
public class InstituicaoEndpoint {
	
	@Autowired
	private InstituicaoService instituicaoService;
	
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE, value="/{id}")
	public Instituicao get(@PathVariable("id") Long id) {
		return instituicaoService.find(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	public void post(@RequestBody Instituicao instituicao) {
		instituicao.setSenha(instituicaoService.senhaCriptrografada(instituicao.getSenha()));
		if(instituicao.getId() == null){
			instituicaoService.save(instituicao);
		}else{
			instituicaoService.update(instituicao);
		}
	}

}
