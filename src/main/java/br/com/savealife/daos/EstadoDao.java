package br.com.savealife.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.savealife.models.Estado;

@Repository
public class EstadoDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public List<Estado> all(){
		return manager.createQuery("select e from Estado e order by e.sigla asc", Estado.class).getResultList();
	}

}
