package br.com.savealife.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;
import br.com.savealife.models.Parceiro;

@Repository
public class ParceiroDao {

	@PersistenceContext
	private EntityManager manager;

	public List<Parceiro> all(Long id) {
		return manager.createQuery("select p from Parceiro p where p.codInstituicao = " + id + " order by p.nome asc",
				Parceiro.class).getResultList();
	}

	public void save(Parceiro parceiro) {
		manager.persist(parceiro);
	}

	public void delete(Parceiro parceiro) {
		manager.remove(parceiro);
	}

	public void update(Parceiro parceiro) {
		manager.merge(parceiro);
	}

	public Parceiro find(Long id) {
		return manager.find(Parceiro.class, id);
	}

	public List<Parceiro> find(Parceiro parceiro) {
		StringBuilder query = new StringBuilder();
		query.append("select p from Parceiro p  where 1=1 ");
		if (parceiro.getNome() != null) {
			query.append(" and p.nome like '%");
			query.append(parceiro.getNome());
			query.append("'%");
		}
		if (parceiro.getCnpj() != null) {
			query.append(" and p.cnpj = '");
			query.append(parceiro.getCnpj());
			query.append("'");
		}
		if (parceiro.getEmail() != null) {
			query.append(" and p.email = '");
			query.append(parceiro.getEmail());
			query.append("'");
		}
		if (parceiro.getSite() != null) {
			query.append(" and p.site = '");
			query.append(parceiro.getSite());
			query.append("'");
		}
		if (parceiro.getTelefone() != null) {
			if (parceiro.getTelefone().getDdd() != null) {
				query.append(" and p.telefone.ddd = ");
				query.append(parceiro.getTelefone().getDdd());
			}
			if (parceiro.getTelefone().getNumero() != null) {
				query.append(" and p.telefone.numero = ");
				query.append(parceiro.getTelefone().getNumero());
			}
		}
		query.append("order by p.nome asc ");

		return manager.createQuery(query.toString(), Parceiro.class).getResultList();
	}

}
