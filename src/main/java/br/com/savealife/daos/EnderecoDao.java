package br.com.savealife.daos;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.savealife.models.Endereco;

@Repository
public class EnderecoDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public void save(Endereco endereco) {
		manager.persist(endereco);
	}
	
	@Transactional
	public void update(Endereco endereco) {
		manager.merge(endereco);
	}
	
}
