package br.com.savealife.daos;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.savealife.models.Atendimento;

@Repository
public class AtendimentoDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public void save(Atendimento atendimento) {
		manager.persist(atendimento);
	}
	
	@Transactional
	public void update(Atendimento atendimento) {
		manager.merge(atendimento);
	}

}
