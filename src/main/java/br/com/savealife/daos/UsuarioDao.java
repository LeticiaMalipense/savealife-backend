package br.com.savealife.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.savealife.models.Usuario;

@Repository
public class UsuarioDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public List<Usuario> all(Usuario usuario) {
		 return manager.createQuery("select u from Usuario u order by u.nome asc", Usuario.class).getResultList();
	}
	
	public List<Usuario> all() {
		 return manager.createQuery("select u from Usuario u order by u.nome asc", Usuario.class).getResultList();
	}
	
}
