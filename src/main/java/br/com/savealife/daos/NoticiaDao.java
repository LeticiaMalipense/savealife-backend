package br.com.savealife.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.savealife.models.Noticia;

@Repository
public class NoticiaDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public List<Noticia> all(Long id){
      return manager.createQuery("select n from Noticia n where n.codInstituicao = "+id+" order by n.data asc", Noticia.class).getResultList();
    }
	
	public void save(Noticia noticia){
		manager.persist(noticia);
	}

	public void delete(Noticia noticia) {
		manager.remove(noticia);
	}

	public void update(Noticia noticia) {
		manager.merge(noticia);
	}
	
	public Noticia find(Long id) {
		return manager.find(Noticia.class, id);
	}

}
