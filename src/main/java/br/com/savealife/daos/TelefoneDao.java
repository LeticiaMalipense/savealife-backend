package br.com.savealife.daos;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.savealife.models.Telefone;

@Repository
public class TelefoneDao {

	@PersistenceContext
	private EntityManager manager;
	
	public void save(Telefone telefone) {
		manager.persist(telefone);
	}
	
	@Transactional
	public void update(Telefone telefone) {
		manager.merge(telefone);
	}
}
