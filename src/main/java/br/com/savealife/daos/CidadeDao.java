package br.com.savealife.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Repository;

import br.com.savealife.models.Cidade;

@Repository
public class CidadeDao {
	
	@PersistenceContext
	private EntityManager manager;
	
	public List<Cidade> listCidadesPorEstado(Long idEstado){
		return manager.createQuery("select c from Cidade c Where c.estado.id = "+idEstado+" order by c.nome asc", Cidade.class).getResultList();
	}


}
