package br.com.savealife.daos;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import br.com.savealife.models.Credencial;
import br.com.savealife.models.Instituicao;

@Repository
public class InstituicaoDao {

	@PersistenceContext
	private EntityManager manager;

	public void save(Instituicao instituicao) {
		manager.persist(instituicao);
	}

	@Transactional
	public void update(Instituicao instituicao) {
		manager.merge(instituicao);
	}

	public List<Instituicao> all() {
		return manager.createQuery("select e from Instituicao i order by i.nome asc", Instituicao.class)
				.getResultList();
	}

	public List<Instituicao> listInstituicaoPorCodigo(Long codigo) {
		return manager.createQuery("select i from Instituicao i where i.id=" + codigo + " order by i.nome asc",
				Instituicao.class).getResultList();
	}

	public Credencial findCredencais(String email, String senha) {
		StringBuilder query = new StringBuilder();
		query.append("select new br.com.savealife.models.Credencial(i.id, tipoIntituicao, i.nome, i.cnpj) ");
		query.append("from Instituicao i where 1=1 ");
		query.append("and UPPER(i.email) = '");
		query.append(email.toUpperCase());
		query.append("' and i.senha = '");
		query.append(senha);
		query.append("'");

		try {
			return manager.createQuery(query.toString(), Credencial.class).getSingleResult();
		} catch (Exception e) {
			return null;
		}
	}

	public Instituicao find(Long id) {
		return manager
				.createQuery("select i from Instituicao i where i.id=" + id + " order by i.nome asc", Instituicao.class)
				.getSingleResult();
	}

}
