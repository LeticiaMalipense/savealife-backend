package br.com.savealife.models;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

@Entity
@Table(name="tb_noticia", schema="dbgeral")  
public class Noticia implements Serializable{
	
	private static final long serialVersionUID = 5702021465275375870L;

	@Id
	@Column(name = "cod", length = 9, nullable = false)	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long cod;
	
	@Column(name = "cod_instituicao")
	private Long codInstituicao;
	
	@Column(name = "data")
	private Date data;
	
	@Transient
	private String dataFormatada;
	
	@Column(name = "tipo_sanguineo")
	private String tipoSanguineo;
	
	@Column(name = "descricao")
	private String descricao;
	
	public Long getCod() {
		return cod;
	}
	public void setCod(Long cod) {
		this.cod = cod;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public String getTipoSanguineo() {
		return tipoSanguineo;
	}
	public void setTipoSanguineo(String tipoSanguineo) {
		this.tipoSanguineo = tipoSanguineo;
	}
	public Long getCodInstituicao() {
		return codInstituicao;
	}
	public void setCodInstituicao(Long codInstituicao) {
		this.codInstituicao = codInstituicao;
	}
	public String getDescricao() {
		return descricao;
	}
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}
	public String getDataFormatada() {
		if(data != null){
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			dataFormatada = formatter.format(data);
		}
		return dataFormatada;
	}
	public void setDataFormatada(String dataFormatada) {
		this.dataFormatada = dataFormatada;
	}
	

}
