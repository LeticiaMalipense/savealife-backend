package br.com.savealife.models;

public class Credencial {
	private Long id;
	
	private Long tipoInstituicao;
	
	private String nome;
	
	private String cnpj;
	
	public Credencial(Long id, Long tipoInstituicao, String nome, String cnpj){
		this.id = id;
		this.tipoInstituicao = tipoInstituicao;
		this.nome = nome;
		this.cnpj = cnpj;
	}

	public Long getTipoInstituicao() {
		return tipoInstituicao;
	}

	public void setTipoInstituicao(Long tipoInstituicao) {
		this.tipoInstituicao = tipoInstituicao;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
