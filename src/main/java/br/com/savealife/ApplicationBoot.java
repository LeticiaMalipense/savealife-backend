package br.com.savealife;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.stereotype.Controller;

@SpringBootApplication
@Controller
@EnableAutoConfiguration
public class ApplicationBoot
{

   public static void main(String[] args)
   {
      SpringApplication.run(ApplicationBoot.class, args);
   }

   @RequestMapping("/")
   @ResponseBody
   public String home()
   {
      return "home";
   }
}
